import os
import time

# Vælg et katalog på din maskine
dir = "C:/users/karthie/desktop/"

filenames = os.listdir(dir)
#print(filenames)

for filename in filenames:
  path = dir + filename
  # Get seconds since Jan 1, 1970
  mod_time = os.path.getmtime(path)
  # Get size in bytes
  size = os.path.getsize(path)

  if os.path.isdir(path):
    print(filename,"<dir>")
  else:
    print(filename.ljust(25),str(size).rjust(9),time.ctime(mod_time))
