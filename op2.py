import os
import time

# Vælg et katalog på din maskine
dir = "C:/users/karthie/desktop/"

filenames = os.listdir(dir)
# print(filenames)

for filename in filenames:
    path = dir + filename
    # Get seconds since Jan 1, 1970
    mod_time = os.path.getmtime(path)
    # Get size in bytes
    size = os.path.getsize(path)

    if os.path.isdir(path):
        print(filename, "<dir>")
    else:
        print(filename.ljust(25), str(size).rjust(9), time.ctime(mod_time))

# Calculate and Display Directory Size – Python (https://codezup.com/calculate-and-display-directory-size-python/)
print("\n")

directory_size = 0

fsizedicr = {'Bytes': 1,
             'Kilobytes': float(1) / 1024,
             'Megabytes': float(1) / (1024 * 1024),
             'Gigabytes': float(1) / (1024 * 1024 * 1024)}

for (path, dirs, files) in os.walk(dir):
    for file in files:
        filename = os.path.join(path, file)
        directory_size += os.path.getsize(filename)

for key in fsizedicr:
    print("Folder Size: " + str(round(fsizedicr[key] * directory_size, 2)) + " " + key)

# Find the oldest and newest file (https://gist.github.com/benhosmer/4634721)
print("\n")
os.chdir(dir)
files = sorted(os.listdir(os.getcwd()), key=os.path.getmtime)

oldest = files[0]
newest = files[-1]

print ("Oldest File:", oldest)
print ("Newest File:", newest)
#print ("All by modified oldest to newest:", files)

# Check if the directory exists or not
dir_exist = os.path.exists(dir)
print("Directory exists or not:", dir, "-->", dir_exist)

