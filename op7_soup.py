import urllib
import urllib.request
import re
from bs4 import BeautifulSoup
import requests

url = "https://mad.eaaa.dk/epages/43530_1036576.sf/da_DK/?ObjectPath=/Shops/43530_1036576/Categories/Category1"
req = urllib.request.Request(url)
f = urllib.request.urlopen(req)

content = f.read()

s = BeautifulSoup(content, "html.parser")

# print(s.prettify())         # laver pæn indrykning

# print(s.get_text())	     # fjerner html tags

# GET Food name
for link in s.find_all('a'):
    #print(link.get('title'))
    if link.get('title') == "Gå til produkt":
         print(link.get_text())

# GET Food prices (https://stackoverflow.com/questions/44575211/python-3-bs4-how-to-select-span-value-under-certain-divs)
for item in s.findAll('span', attrs={'itemprop': 'price'}):
    print(item.text.strip())