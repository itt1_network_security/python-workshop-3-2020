#apt install openssh-server
#sudo nano /etc/ssh/sshd_config (enable port = 22)
#sudo service ssh start

import paramiko

hostname = "192.168.8.148"
username = "kali"
password = "kali"
port = 22

client = paramiko.SSHClient()
client.load_system_host_keys()
client.set_missing_host_key_policy(paramiko.WarningPolicy())

client.connect(hostname, port=port, username=username, password=password)

stdin, stdout, stderr = client.exec_command("ls -lsa")
print(stdout.read().decode("utf-8")) 
